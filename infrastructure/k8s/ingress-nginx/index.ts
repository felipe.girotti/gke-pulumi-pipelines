import * as k8s from '@pulumi/kubernetes';
import { namespace } from './namespace';
import * as pulumi from '@pulumi/pulumi';
import { clusterProvider } from '../../gcp/gke';

const config = new pulumi.Config();
new k8s.helm.v3.Chart('ingress-nginx', {
    chart: 'ingress-nginx',
    version: config.require('ingressNginxChartVersion'),
    fetchOpts: {
        repo: 'https://kubernetes.github.io/ingress-nginx',
    },
    namespace: namespace.metadata.name,
    values: {
        controller: {
            ingressClass: 'ingress-nginx',
            autoscaling: {
                enabled: true,
            },
            resources: {
                limits: {
                    cpu: '250m',
                    memory: '512Mi',
                },
                requests: {
                    cpu: '250m',
                    memory: '512Mi',
                },
            },
        },
    },
}, {
    provider: clusterProvider
});

