import * as k8s from '@pulumi/kubernetes';
import * as k8sx from '@pulumi/kubernetesx';
import { RedisServiceDeployment } from './redis';
import { namespace } from './namespace';
import { clusterProvider } from '../../gcp/gke';
import * as config from './config';

const redisLeader = new RedisServiceDeployment('redis-leader', {
    image: 'docker.io/redis:6.0.5',
    namespace: namespace.metadata.name
});
const redisReplica = new RedisServiceDeployment('redis-replica', {
    image: 'gcr.io/google_samples/gb-redis-follower:v2',
    namespace: namespace.metadata.name,
    redisLeaderService: redisLeader.service,
});

const pod = new k8sx.PodBuilder({
    containers: [
        {
            name: 'frontend',
            image: 'gcr.io/google_samples/gb-frontend:v5',
            env: [
                {
                    name: 'GET_HOSTS_FROM',
                    value: 'env',
                },
                {
                    name: 'REDIS_LEADER_SERVICE_HOST',
                    value: redisLeader.service.metadata.name,
                },
                {
                    name: 'REDIS_FOLLOWER_SERVICE_HOST',
                    value: redisReplica.service.metadata.name,
                }
            ],
            resources: {
                requests: {
                    cpu: '250m', // On Autopilot the minimum of CPU per pod is 250m
                    memory: '512Mi' // On Autopilot the minimum of MEM per pod is 512Mi
                },
                limits: {
                    cpu: '250m',
                    memory: '512Mi',
                },
            },
            ports: [
                {
                    containerPort: 80,
                }
            ]
        }
    ]
});

const deployment = new k8sx.Deployment('guestbook', {
    metadata: {
        namespace: namespace.metadata.name,
    },
    spec: pod.asDeploymentSpec({ replicas: config.guestbookConfig.replicas }),
}, {
    provider: clusterProvider,
});

const service = deployment.createService({
    ports: [
        {
            port: 80,
        },
    ],
});

new k8s.networking.v1.Ingress('guestbook', {
    metadata: {
        namespace: namespace.metadata.name,
        annotations: {
            'kubernetes.io/ingress.class': 'ingress-nginx',
        },
    },
    spec: {
        rules: [
            {
                host: config.guestbookConfig.host,
                http: {
                    paths: [
                        {
                            path: '/',
                            pathType: 'ImplementationSpecific',
                            backend: {
                                service: {
                                    name: service.metadata.name,
                                    port: {
                                        number: service.spec.ports[0].port,
                                    },
                                },
                            },
                        },
                    ],
                },
            },
        ]
    }
}, {
    provider: clusterProvider,
});

export const response = {
    host: config.guestbookConfig.host,
};
