import * as pulumi from '@pulumi/pulumi';

interface GuestBookConfiguration {
    replicas: number;
    host: string;
}

const config = new pulumi.Config();
const guestbookConfig: GuestBookConfiguration = config.requireObject('guestbook');

export { config, guestbookConfig };
