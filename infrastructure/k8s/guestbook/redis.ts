import * as k8sx from '@pulumi/kubernetesx';
import * as pulumi from '@pulumi/pulumi';
import { namespace } from './namespace';
import { clusterProvider } from '../../gcp/gke';
import * as k8s from  '@pulumi/kubernetes';

export interface RedisServiceDeploymentArgs {
    image: string;
    namespace: pulumi.Input<string>;
    redisLeaderService?: k8sx.Service;
}

export class RedisServiceDeployment extends pulumi.ComponentResource {
    public readonly service: k8sx.Service;
    constructor(name: string, args: RedisServiceDeploymentArgs, opts?: pulumi.ComponentResourceOptions) {
        super("guestbook:service:RedisServiceDeployment", name, {}, opts);
        const env: k8s.types.input.core.v1.EnvVar[] = [];
        if (args.redisLeaderService) {
            env.push({
                name: 'GET_HOSTS_FROM',
                value: 'env'
            });
            env.push({
                name: 'REDIS_LEADER_SERVICE_HOST',
                value: args.redisLeaderService.metadata.name,
            });
        }
        const podSpec = new k8sx.PodBuilder({
            containers: [
                {
                    name: name,
                    image: args.image,
                    env,
                    resources: {
                        requests: {
                            cpu: '250m', // On Autopilot the minimun of CPU per pod is 250m
                            memory: '512Mi' // On Autopilot the minimum of MEM per pod is 512Mi
                        },
                        limits: {
                            cpu: '250m',
                            memory: '512Mi',
                        },
                    },
                    ports: [
                        {
                            containerPort: 6379,
                        },
                    ],
                },
            ]
        });

        const deployment = new k8sx.Deployment(name, {
            spec: podSpec.asDeploymentSpec({replicas: 1}),
            metadata: {
                namespace: namespace.metadata.name,
            }
        }, {
            provider: clusterProvider,
            dependsOn: [ namespace ],
        });

        this.service = deployment.createService({

            ports: [
                {
                    port: 6379,
                },
            ],
        });
    }
}
