import * as k8sx from '@pulumi/kubernetesx';
import { namespace } from './namespace';
import * as config from './config';
import { clusterProvider } from '../../gcp/gke';
import { pod } from './pod';

export const deployment = new k8sx.Deployment('greeting', {
    metadata: {
        namespace: namespace.metadata.name,
    },
    spec: pod.asDeploymentSpec({ replicas: config.greetingConfig.replicas.min }),
}, {
    provider: clusterProvider,
});
