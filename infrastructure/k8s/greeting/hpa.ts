import * as k8s from '@pulumi/kubernetes';
import { namespace } from './namespace';
import * as config from './config';
import { deployment } from './deployment'

new k8s.autoscaling.v2beta2.HorizontalPodAutoscaler('greeting', {
    metadata: {
        namespace: namespace.metadata.name
    },
    spec: {
        minReplicas: config.greetingConfig.replicas.min,
        maxReplicas: config.greetingConfig.replicas.max,
        scaleTargetRef: {
            apiVersion: deployment.apiVersion,
            kind: deployment.kind,
            name: deployment.metadata.name,
        },
        metrics: [
            {
                type: 'Resource',
                resource: config.greetingConfig.autoscaling.cpu,
            },
            {
                type: 'Resource',
                resource: config.greetingConfig.autoscaling.memory,
            },
        ],
    },
});
