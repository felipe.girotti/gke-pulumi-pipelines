import * as pulumi from '@pulumi/pulumi';
import * as k8s from '@pulumi/kubernetes';

interface GreetingConfiguration {
    replicas: {
        min: number;
        max: number;
    };
    autoscaling: {
        cpu: k8s.types.input.autoscaling.v2beta2.ResourceMetricSource;
        memory: k8s.types.input.autoscaling.v2beta2.ResourceMetricSource;
    }
    host: string;
    version: string;
}

interface RegistryConfiguration {
    url: string;
    username: string;
    password: string;
}

const config = new pulumi.Config();
const greetingConfig: GreetingConfiguration = config.requireObject('greeting');
const registryConfig: RegistryConfiguration = config.requireObject('registry');

export { config, greetingConfig, registryConfig };
