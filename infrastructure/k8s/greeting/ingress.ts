import * as k8s from '@pulumi/kubernetes';
import { namespace } from './namespace';
import * as config from './config';
import { clusterProvider } from '../../gcp/gke';
import { service } from './service';

new k8s.networking.v1.Ingress('greeting', {
    metadata: {
        namespace: namespace.metadata.name,
        annotations: {
            'kubernetes.io/ingress.class': 'ingress-nginx',
        },
    },
    spec: {
        rules: [
            {
                host: config.greetingConfig.host,
                http: {
                    paths: [
                        {
                            path: '/',
                            pathType: 'ImplementationSpecific',
                            backend: {
                                service: {
                                    name: service.metadata.name,
                                    port: {
                                        number: service.spec.ports[0].port,
                                    },
                                },
                            },
                        },
                    ],
                },
            },
        ]
    }
}, {
    provider: clusterProvider,
});
