import * as k8sx from '@pulumi/kubernetesx';
import * as config from './config';
import {regSecret} from "./regsecret";

export const pod = new k8sx.PodBuilder({
    imagePullSecrets: [{
        name: regSecret.metadata.name,
    }],
    containers: [
        {
            name: 'nginx',
            image: `registry.gitlab.com/felipe.girotti/gke-pulumi-pipelines/nginx:${config.greetingConfig.version}`,
            imagePullPolicy: 'Always',
            env: [
                {
                    name: 'PHP_FPM_HOST',
                    value: 'localhost'
                }
            ],
            resources: {
                requests: {
                    cpu: '100m', // On Autopilot the minimum of CPU per pod is 250m, 100 for nginx 150 to php
                    memory: '128Mi' // On Autopilot the minimum of MEM per pod is 512Mi, 128 for nginx 384 to php
                },
                limits: {
                    cpu: '100m',
                    memory: '128Mi',
                },
            },
            ports: [
                {
                    containerPort: 80,
                }
            ]
        },
        {
            name: 'php',
            image: `registry.gitlab.com/felipe.girotti/gke-pulumi-pipelines/php:${config.greetingConfig.version}`,
            imagePullPolicy: 'Always',
            resources: {
                requests: {
                    cpu: '150m', // On Autopilot the minimum of CPU per pod is 250m, 100 for nginx 150 to php
                    memory: '384Mi' // On Autopilot the minimum of MEM per pod is 512Mi, 128 for nginx 384 to php
                },
                limits: {
                    cpu: '150m',
                    memory: '385Mi',
                },
            },
            ports: [
                {
                    containerPort: 9000,
                }
            ]
        }
    ]
});
