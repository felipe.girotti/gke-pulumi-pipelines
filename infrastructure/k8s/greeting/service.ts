import { deployment } from './deployment';

const service = deployment.createService({
    ports: [
        {
            port: 80,
        },
    ],
});

const serviceName = service.metadata.name;

export { service, serviceName };
