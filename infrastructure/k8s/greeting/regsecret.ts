import * as k8s from '@pulumi/kubernetes';
import { namespace } from './namespace';
import { clusterProvider } from '../../gcp/gke';
import * as config from './config';

const dockerconfigjson = JSON.stringify({
    auths: {
        [config.registryConfig.url]: {
            username: config.registryConfig.username,
            password: config.registryConfig.password,
            auth: Buffer.from(`${config.registryConfig.username}:${config.registryConfig.password}`).toString('base64')
        },
    },
});

export const regSecret = new k8s.core.v1.Secret('gitlab-regsecret', {
    metadata: {
        namespace: namespace.metadata.name,
    },
    type: 'kubernetes.io/dockerconfigjson',
    data: {
        '.dockerconfigjson': Buffer.from(dockerconfigjson).toString('base64')
    }
}, {
    provider: clusterProvider
});
