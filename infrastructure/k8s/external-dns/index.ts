import * as k8s from '@pulumi/kubernetes';
import { namespace } from './namespace';
import * as pulumi from '@pulumi/pulumi';
import { clusterProvider } from '../../gcp/gke';

const config = new pulumi.Config();
interface ExternalDnsConfig {
    domainFilters: string[];
    version: string;
}
const externalDnsConfig: ExternalDnsConfig = config.requireObject('externalDns');

new k8s.helm.v3.Chart('external-dns', {
    chart: 'external-dns',
    version: externalDnsConfig.version,
    fetchOpts: {
        repo: 'https://charts.bitnami.com/bitnami',
    },
    namespace: namespace.metadata.name,
    values: {
        provider: 'cloudflare',
        sources: ['ingress'],
        domainFilters: externalDnsConfig.domainFilters,
        policy: 'sync',
        cloudflare: {
            apiToken: config.requireSecret('externalDnsCloudFlareToken'),
        },
        resources: {
            requests: {
                cpu: '250m',
                memory: '512Mi',
            },
            limits: {
                cpu: '250m',
                memory: '512Mi',
            },
        },
    },
}, {
    provider: clusterProvider
});
