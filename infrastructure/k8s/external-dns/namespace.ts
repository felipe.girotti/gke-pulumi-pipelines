import * as k8s from '@pulumi/kubernetes';
import {clusterProvider} from '../../gcp/gke';

export const namespace = new k8s.core.v1.Namespace('external-dns', {
    metadata: {
        name: 'external-dns',
    }
}, {
    provider: clusterProvider,
});
