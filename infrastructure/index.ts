export * from './gcp/gke';
require('./k8s/ingress-nginx');
require('./k8s/external-dns');

export * from './k8s/guestbook';
export * from './k8s/greeting';
