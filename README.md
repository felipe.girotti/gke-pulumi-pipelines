# Gke Autopilot Pulumi Pipelines

This is part of the articles on Medium to firsts steps to use [Pulumi](https://www.pulumi.com/) as infrastructure as code with [GKE Autopilot](https://cloud.google.com/blog/products/containers-kubernetes/introducing-gke-autopilot)

- [First Article](https://medium.com/@felipegirotti/gke-autopilot-kubernetes-cluster-with-pulumi-infrastructure-as-code-c74aqe8f7ee0f) - [Git Tag](https://gitlab.com/felipe.girotti/gke-pulumi-pipelines/-/tree/v0.1.0)
- [Second Article](https://medium.com/@felipegirotti/install-ingress-nginx-and-externaldns-with-pulumi-on-gke-autopilot-6417c13f99ce) - [Git Tag](https://gitlab.com/felipe.girotti/gke-pulumi-pipelines/-/tree/v0.2.0)
- [Third Article](https://medium.com/@felipegirotti/gitlab-pipelines-build-tests-and-deploy-private-images-gke-pulumi-480d5d56759b) - [Git Tag](https://gitlab.com/felipe.girotti/gke-pulumi-pipelines/-/tree/v0.3.0)
